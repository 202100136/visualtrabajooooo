﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProyectoBB.Modelo;

namespace ProyectoBB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private List<Usuario> usuarios = new List<Usuario>();
        private Usuario UsuarioEdit;
        public MainWindow()
        {
            InitializeComponent();
            tblUsuarios.ItemsSource = usuarios;
        }
        private void ButtonGuardar(Object sender, RoutedEventArgs e)
        {
            Usuario usuario = new Usuario();

            usuario.Id = 0;
            usuario.Nombres = txtNombres.Text;
            usuario.Apellidos = txtApellidos.Text;
            usuario.Correo = txtCorreo.Text;
            usuarios.Add(usuario);
            tblUsuarios.Items.Refresh();

            DataBaseContext conexion = new DataBaseContext();
            conexion.Usuarios.Add(usuario);
            conexion.SaveChanges();
            conexion.Dispose();
        }

        private void tblUsuarios_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Usuario usuarioTEMPORAL = (Usuario)tblUsuarios.SelectedItem;
            if (usuarioTEMPORAL != null)
            {
                return;
            }
            txtNombres.Text = UsuarioEdit.Nombres;
            txtApellidos.Text = UsuarioEdit.Apellidos;
            txtCorreo.Text = UsuarioEdit.Correo;//ajkshndjia
            //hola
        }

        private void ButtonActualizar(object sender, RoutedEventArgs e)
        {
            if (UsuarioEdit == null)
            {
                return;
            }
            Usuario usuario = UsuarioEdit;
            usuario.Nombres = txtNombres.Text;
            usuario.Apellidos = txtApellidos.Text;
            usuario.Correo = txtCorreo.Text;
            tblUsuarios.Items.Refresh();
            DataBaseContext Conexion = new DataBaseContext();
            Conexion.Usuarios.Update(usuario);
            Conexion.Dispose();
            UsuarioEdit=null;
        }
    }
}
